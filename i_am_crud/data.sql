CREATE TABLE IF NOT EXISTS `phonebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 ;

INSERT INTO `phonebook` (`name`, `email`, `mobile`) VALUES
('Zeus', 'zeus@olimp.example.net', '556444332'),
('Anthena', 'anthena@olimp.example.net', '1122334455'),
('Jupiter', 'jupiter@olimp.example.net', '12'),
('Venus', 'venus@olimp.example.net', '1');