<?php
    require("db.php");

    $dbh = db_connect();
    $sql_stmt = "SELECT * FROM phonebook";
    $result = mysqli_query($dbh, $sql_stmt);
    if (!$result){
        die("Database access failed: " . mysqli_error($dbh));
    }
    $rows = mysqli_num_rows($result);
    if ($rows) {
        while ($row = mysqli_fetch_array($result)) {
            echo 'ID:       ' . $row['id'] . "\n";
            echo 'Name:     ' . $row['name'] . "\n";
            echo 'Email:    ' . $row['email'] . "\n";
            echo 'Mobile:   ' . $row['mobile'] . "\n";
        }
    }
    mysqli_close($dbh);
?>
