<?php
    $DB_HOSTNAME='127.0.0.1';
    $DB_USERNAME='phonebook';
    $DB_PASSWORD='***';
    $DB_DATABASE='phonebook';

    function db_connect(){
        global $DB_DATABASE, $DB_HOSTNAME, $DB_USERNAME, $DB_PASSWORD;

        $dbh = mysqli_connect($DB_HOSTNAME, $DB_USERNAME, $DB_PASSWORD);

        if (!$dbh) {
            die("Unable to connect to MySQL");
        }
        if (!mysqli_select_db($dbh, $DB_DATABASE)){
            die("Unable to select database: " . mysqli_error($dbh));
        }
        return $dbh;
    }
?>