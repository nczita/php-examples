<?php
/**
 * @package Hello_Teb
 * @version 1.0.0
 */
/*
Plugin Name: Hello Teb
Plugin URI: https://bitbucket.org/nczita/php-examples/src/master/teb-hello
Description: This is just a plugin, it should make you happy. When activated you will randomly see a text from fortunes in the upper right of your admin screen on every page.
Author: Luke Dziewanowski
Version: 1.0.0
Author URI: https://saltsoft.pl
*/

function hello_teb_get_fortune() {
    /** These are the fortunes */
    $fortunes = "Anything that is good and useful is made of chocolate.
Do not drink coffee in early A.M.  It will keep you awake until noon.
Home on the Range was originally written in beef-flat.
I never pray before meals -- my mom's a good cook.
Is there life before breakfast?
My weight is perfect for my height -- which varies.
The only thing better than love is milk.
I am two with nature.
I have a rock garden. Last week three of them died.
A child of five could understand this! Fetch me a child of five.
Get Revenge! Live long enough to be a problem for your children!";

    // Here we split it into lines.
    $fortunes = explode("\n", $fortunes);

    // And then randomly choose a line.
    return wptexturize( $fortunes[ mt_rand( 0, count( $fortunes ) - 1 ) ] );
}

// This just echoes the chosen line, we'll position it later.
function hello_teb() {
    $chosen = hello_teb_get_fortune();
    $lang   = '';
    if ( 'en_' !== substr( get_user_locale(), 0, 3 ) ) {
        $lang = ' lang="en"';
    }

    printf(
        '<p id="teb"><span class="screen-reader-text">%s </span><span dir="ltr"%s>%s</span></p>',
        __( 'Here is fortune for you:', 'hello-teb' ),
        $lang,
        $chosen
    );
}

// Now we set that function up to execute when the admin_notices action is called.
add_action( 'admin_notices', 'hello_teb' );

// We need some CSS to position the paragraph.
function teb_css() {
    echo "
    <style type='text/css'>
    #teb {
        float: right;
        padding: 5px 10px;
        margin: 0;
        font-size: 12px;
        line-height: 1.6666;
    }
    .rtl #teb {
        float: left;
    }
    .block-editor-page #teb {
        display: none;
    }
    @media screen and (max-width: 782px) {
        #teb,
        .rtl #teb {
            float: none;
            padding-left: 0;
            padding-right: 0;
        }
    }
    </style>
    ";
}

add_action( 'admin_head', 'teb_css' );
